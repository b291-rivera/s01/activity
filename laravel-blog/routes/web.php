<?php

use Illuminate\Support\Facades\Route;

// link the PostController file.
use App\Http\Controllers\PostController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// define a route wherein a view to create a post will be returned to the user.
Route::get('/posts/create', [PostController::class, 'create']);
// Modified

// define a route wherein form data will be sent via POST method to the /posts URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

Route::get('/posts', [PostController::class, 'index']);

Route::get('/', [PostController::class, 'featuredPosts']);

Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route wherein showing a specific post with the matching url parameter ID will be returned to the user
Route::get('/posts/{id}', [PostController::class, 'show']);

Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// Activity S03
Route::put('/posts/{id}', [PostController::class, 'update'])->name('posts.update');

// define a route that will overwrite an existing post with matching URL parameter ID via PUT method

    // Discussion code
// Route::put('/posts/{id}', [PostController::class, 'update']);

// define a route that will delete a post of the matching URL parameter 
// Route::delete('/posts/{id}', [PostController::class, 'archive'])

Route::delete('/posts/{id}', [PostController::class, 'archive'])->name('posts.archive');

// define a route that will allow user to like a posts.
Route::put('/posts/{id}/like', [PostController::class, 'like']);

Route::post('/posts/{id}/comment', [PostController::class, 'comment'])->name('posts.comment');;