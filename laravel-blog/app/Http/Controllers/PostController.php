<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// access the authenticated user via Auth Facade
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        if(Auth::user()) {
            // instantiate a new Post object from the Post Model
            $post = new Post;
            // define the properties of the $post object using the received form data.
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key for the user_id of the new post.
            $post->user_id = (Auth::user()->id);

            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    // action that will return a view showing all the blog posts.
    public function index()
    {
        $posts = Post::where('isActive', true)->get();
        return view('posts.index')->with('posts', $posts);
    }
    public function featuredPosts(Request $request)
    {
        $posts = Post::with('user')->where('isActive', true)->inRandomOrder()->limit(3)->get();
        return view('welcome', compact('posts'));
    }

    // action for showing only the posts authored by the authenticated user.
    public function myPosts()
    {
        if(Auth::user()){
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.edit', ['post' => $post]);
    }

    //Activity s03
    public function update(Request $request, $id)
    {

        if(Auth::user())
        {
           $post = Post::find($id); 

           $validatedData = $request->validate([
                'title' => 'required|max:255',
                'content' => 'required',
            ]);

           $post->title = $validatedData['title'];
           $post->content = $validatedData['content'];
           $post->save();

           return redirect('/');

        } else {
            return redirect('/login');
        }

    }


        // Discussion code

//     public function update(Request $request, $id)
//     {
//         $post = Post::find($id);

//         if(Auth::user()->id == $post->user_id){
//             $post->title = $request->input('title');
//             $post->content = $request->input('content');
//             $post->save();
//         }
//         return redirect('/posts');
//     }


        // public function destroy($id)
        // {
        //     $post = Post::find($id);

        //     if(Auth::user()->id == $post->user_id){
        //         $post->delete();
        //     }

        //     return redirect('/posts');
        // }

        public function archive($id)
        {
            $post = Post::find($id);

            $post->isActive = false;
            $post->save();
            

            return redirect('/posts')->with('success', 'Post archived successfully!');
        }

        public function like($id)
        {
            $post = Post::find($id);
            $user_id = Auth::user()->id;

            // if authenticated user is not the post author
            if($post->user_id != $user_id ){
                // checks if a post like has been made by the logged in user before
                if($post->likes->contains("user_id", $user_id)) {
                    // deletes the like made by the user to unlike the post.
                    PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
                } else {
                    // to instantiate a new $postLike object
                    $postLike = new PostLike;
                    $postLike->post_id = $post->id;
                    $postLike->user_id = $user_id;

                    // save the $postLike object into the database
                    $postLike->save();
                }

                // redirect user back to the original post.
                return redirect("/posts/$id");
            }
        }

        public function comment(Request $request, $id)
        {
            $user_id = Auth::user()->id;
            $post = Post::find($id);

            $comment = new PostComment;
            $comment->post_id = $post->id;
            $comment->user_id = $user_id;
            $comment->content = $request->input('content');
            $comment->save();

            return redirect("/posts/$id");

        }
}

