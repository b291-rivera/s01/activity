@extends('layouts.app')

@section('content')

	<form method="POST" action="{{ route('posts.update', $post->id) }}">
		@csrf
		@method('PUT')
		<div>
		            <label for="title">Title:</label>
		            <input type="text" name="title" id="title" class="form-control" value="{{ $post->title }}">
		        </div>
		        <div>
		            <label for="content">Content:</label>
		            <textarea class="form-control" id="content" name="content" rows="3" >{{$post->content}}</textarea>
		        </div>
		        <div class="mt-2">
		        	<button type="submit" class="btn btn-primary">Update Post</button>
		        </div>
		        
	</form>
@endsection