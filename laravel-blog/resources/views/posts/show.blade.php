@extends('layouts.app')


@section('content')

	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>


			@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">

					@method('PUT')
					@csrf

					@if($post->likes->contains("user_id", Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
					@else
						<button type="submit" class="btn btn-success">Like</button>
					@endif
				</form>
			@endif

			<!-- Button trigger modal -->
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
			  Add Comment
			</button>

			<!-- Modal -->
			<!-- Comment Modal -->
			<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-header">
			                <h5 class="modal-title" id="commentModalLabel">Add Comment</h5>
			                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			            </div>
			            <div class="modal-body">
			                <form method="post" action="{{ route('posts.comment', $post->id) }}">
			                    @method('POST')
			                    @csrf
			                    <div class="mb-3">
			                        <label for="content" class="form-label">Comment:</label>
			                        <textarea name="content" id="content" class="form-control" rows="3" required></textarea>
			                    </div>
			                    <button type="submit" class="btn btn-primary">Submit</button>
			                </form>
			                <hr>
			            </div>
			        </div>
			    </div>
			</div>
			<h6 class="mt-2">Comments:</h6>
			@foreach ($post->comments as $comment)
			    <div class="card mb-3">
			        <div class="card-body">
			            <p class="card-text">{{ $comment->content }}</p>
			            <p class="card-text"><small class="text-muted">{{ $comment->created_at->diffForHumans() }}</small></p>
			        </div>
			    </div>
			@endforeach




			<div class="mt-3"> 
				<a href="/posts" class="card-link">View all posts</a>

			</div>
		</div>
	</div>


@endsection